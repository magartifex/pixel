package lib

import (
	"log"
	"net/http"
)

func SetError(status int, wr http.ResponseWriter, message string) {
	log.Println(message)
	wr.WriteHeader(status)
	_, err := wr.Write([]byte(message))
	if err != nil {
		log.Println("lib.SetError #1:", err)
		return
	}
}
