package processor

import (
	"fmt"
	"log"

	"bitbucket.org/magartifex/pixel/config"
	"bitbucket.org/magartifex/pixel/server"
)

func Start() {
	err := config.Load()
	if err != nil {
		log.Println("processor.Start #1:", err)
		return
	}
	fmt.Println("config Load")

	err = server.Start()
	if err != nil {
		log.Println("processor.Start #2:", err)
		return
	}
}
