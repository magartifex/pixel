import sys
import json
import time

try:
    import board
    import neopixel
except ImportError:
    exit('Import error')
except NotImplementedError:
    exit('Wrong device version')

PIXEL_PIN = board.D18
ORDER = neopixel.RGB
DEFAULT_BRIGHTNESS = 0.5

try:
    incomePixelList = json.loads(sys.argv[1])
except IndexError:
    exit('Empty input')
except json.JSONDecodeError:
    exit('Json is invalid')

num_pixels = len(incomePixelList)
pixels = neopixel.NeoPixel(PIXEL_PIN, num_pixels, brightness=DEFAULT_BRIGHTNESS, auto_write=False,
                           pixel_order=ORDER)


def check_pixel_range(pixel):
    int_pixel = int(pixel)
    if 0 <= int_pixel <= 255:
        return int_pixel
    else:
        return 0


for pixel in incomePixelList:
    try:
        pixel_colors = pixel['colors']
        pixel_id = pixel['id']
    except NameError:
        continue
    for color in pixel_colors:
        try:
            pixels[pixel['id']] = (check_pixel_range(color['red']), check_pixel_range(color['green']),
                                   check_pixel_range(color['blue']))
            try:
                delay = pixel['time']
            except NameError:
                delay = 0
            time.sleep(delay)
            pixels.show()
        except NameError:
            continue
exit(0)
