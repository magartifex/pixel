package picture

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"strings"

	"bitbucket.org/magartifex/pixel/lib"
)

func Set(wr http.ResponseWriter, req *http.Request) {
	body, err := getBody(req)
	if err != nil {
		lib.SetError(http.StatusInternalServerError, wr, "picture.Set #1: "+err.Error())
		return
	}

	cmd := exec.Command("bash", "-c", fmt.Sprintf("sudo python3 ./pixel.py '%s'", string(body)))

	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	err = cmd.Run()
	if err != nil {
		lib.SetError(
			http.StatusInternalServerError,
			wr,
			fmt.Sprintf("picture.Set #2:\n%s\n%s\n%s\n\n", stderr.String(), err.Error(), strings.Join(cmd.Args, "\n")),
		)
		return
	}
}

func getBody(req *http.Request) (body []byte, err error) {
	err = req.ParseForm()
	if err != nil {
		log.Println("picture.getBody #1:", err)
		return
	}

	body, err = ioutil.ReadAll(req.Body)
	if err != nil {
		log.Println("picture.getBody #2:", err)
		return
	}

	return
}
