package main

import (
	"log"

	"bitbucket.org/magartifex/pixel/processor"
)

func main() {
	log.Println("Start")
	processor.Start()
	log.Println("Stop")
}
