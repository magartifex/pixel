package config

import "bitbucket.org/magartifex/libs"

var JSON json

func Load() error {
	return libs.LoadJSON("./config.json", &JSON)
}
