package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"bitbucket.org/magartifex/pixel/lib"

	"bitbucket.org/magartifex/pixel/config"
	"bitbucket.org/magartifex/pixel/picture"
	"bitbucket.org/magartifex/pixel/types"
)

func Start() error {
	http.HandleFunc("/", work)
	http.HandleFunc("/picture/set", picture.Set)
	return http.ListenAndServe(fmt.Sprintf(":%d", config.JSON.Port), nil)
}

func work(wr http.ResponseWriter, req *http.Request) {
	pictures := [900]types.Pixel{}
	for key := range pictures {
		pictures[key] = generate(key)
	}

	bPicture, err := json.Marshal(pictures)
	if err != nil {
		lib.SetError(http.StatusInternalServerError, wr, "server.work #1: "+err.Error())
		return
	}

	resp, err := http.Post(
		fmt.Sprintf("http://127.0.0.1:%d/picture/set", config.JSON.Port),
		"application/json",
		bytes.NewReader(bPicture),
	)
	if err != nil {
		lib.SetError(http.StatusInternalServerError, wr, "server.work #2: "+err.Error())
		return
	}

	wr.WriteHeader(resp.StatusCode)
}

func generate(id int) types.Pixel {
	rand.Seed(time.Now().UnixNano())
	return types.Pixel{
		ID: id,
		Colors: []types.Color{
			{Red: rand.Intn(255), Green: rand.Intn(255), Blue: rand.Intn(255)},
		},
		Time: 0,
	}
}
