package types

type Pixel struct {
	ID     int     `json:"id"`
	Colors []Color `json:"colors"`
	Time   int64   `json:"time"`
}

type Color struct {
	Red   int `json:"red"`
	Green int `json:"green"`
	Blue  int `json:"blue"`
}
